package com.application.myApplication.validation;

public class ValidationConstant {
	
	final public static String ERROR_MSG_NO_SUCH_USER = "No Such User";
	final public static String ERROR_CODE_NO_SUCH_USER = "1";
	
	final public static String ERROR_MSG_INCORRECT_PASSWORD = "Incorrect password";
	final public static String ERROR_CODE_INCORRECT_PASSWORD = "2";
	
	final public static String ERROR_MSG_USER_ALREDY_EXITS = "User alredy exits";
	final public static String ERROR_CODE_USER_ALREDY_EXITS = "3";
}
